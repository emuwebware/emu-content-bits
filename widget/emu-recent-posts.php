<?php

/*
Emu Widget: Emu Recent Posts
Emu Widget Class: emuRecentPosts
Emu Widget Description: Add recent posts
*/


/**
 * emuRecentPosts Class
 */
class emuRecentPosts extends WP_Widget
{
	public $menuClass;

	/** constructor */
	function emuRecentPosts()
	{
		global $emuBuild;

		parent::WP_Widget( false, $name = 'Emu Recent Posts' );

	}

    function widget($args, $instance) {

		global $wpdb, $blog_id; extract( $args );

		$title 					= apply_filters( 'widget_title', @$instance['title'] );
		$add_more				= @$instance['add_more'] == 'yes';
		$term					= @$instance['term'];
		$insert_before_item		= @$instance['insert_before_item'];
		$insert_after_item		= @$instance['insert_after_item'];
		$insert_before_group	= @$instance['insert_before_group'];
		$insert_after_group		= @$instance['insert_after_group'];
		$number_items			= @$instance['number_items'];

		echo $before_widget;
		if ( $title ) echo $before_title . $title . $after_title;

		$term = explode( '|', $term);

		if( count( $term ) > 1 )
		{
			// list a specific category
			$term_id = $term[1];
			$taxonomy = $term[0];

			$taxonomy = get_taxonomy( $taxonomy );
			$term = get_term_by( 'id', $term_id, $taxonomy->name );

			$args = array( $taxonomy->name => $term->term_id, 'post_type' => $taxonomy->object_type[0] );
		}
		else
		{
			$taxonomy = $term[0];
			$taxonomy = get_taxonomy( $taxonomy );
			$args = array( 'post_type' => $taxonomy->object_type[0] );
		}

		if( $number_items ) $args['numberposts'] = $number_items;

		$posts = get_posts( $args );

		$posts_output = '';

		foreach( $posts as $post )
		{
			$permalink = get_permalink( $post->ID );
			$item_string = '<a href="'.$permalink.'">'.apply_filters('the_title', $post->post_title).'</a>';

			if( $add_more )
				$item_string .= apply_filters( 'emu-recent-posts-read-more', ' - <a href="'.$permalink.'">Read More</a>', $permalink, $post, $instance );

			$posts_output .= $insert_before_item.apply_filters( 'emu-recent-posts-post', $item_string, $post, $instance ).$insert_after_item;
		}

		echo $insert_before_group . $posts_output . $insert_after_group;

		echo $after_widget;

	}

    function update($new_instance, $old_instance) { return $new_instance; }


	function form($instance) {

		$title 					= esc_attr( @$instance['title'] );
		$term					= esc_attr( @$instance['term']);
		$insert_before_item		= esc_attr( @$instance['insert_before_item'] );
		$insert_after_item		= esc_attr( @$instance['insert_after_item'] );
		$insert_before_group	= esc_attr( @$instance['insert_before_group'] );
		$insert_after_group		= esc_attr( @$instance['insert_after_group'] );
		$add_more				= esc_attr( @$instance['add_more'] );
		$number_items			= esc_attr( @$instance['number_items'] );

		$arr_options = array();

		$taxonomies = get_taxonomies( '', 'objects' );

		foreach ( $taxonomies as $taxonomy )
		{
			$arr_options[ $taxonomy->name ] = 'All '.$taxonomy->labels->name;

			$args = array(
			   'type'                     => 'post',
			   'child_of'                 => 0,
			   'orderby'                  => 'name',
			   'order'                    => 'ASC',
			   'hide_empty'               => 1,
			   'hierarchical'             => 1,
			   'taxonomy'                 => $taxonomy->name,
			   'pad_counts'               => false );

			$categories = get_categories( $args );

			foreach( $categories as $category )
			{
				$arr_options[ $taxonomy->name.'|'.$category->term_id ] = '- '.$category->name;
			}


		}


		?>
		<div class="emu-widget-control">
			<div>
				<label for="<?php echo $this->get_field_id('title'); ?>">
				Title (optional):</label>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
			</div>
			<div>
				<label><strong>Category</strong>:</label>
				<?php echo drop_down( $this->get_field_id('term'),$this->get_field_name('term'), 'widefat', $term, $arr_options, 'All' ) ?>
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('insert_before_item'); ?>">Insert <em>before</em> each item:</label>
				<input class="widefat" id="<?php echo $this->get_field_id('insert_before_item'); ?>" name="<?php echo $this->get_field_name('insert_before_item'); ?>" type="text" value="<?php echo $insert_before_item; ?>" />
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('insert_after_item'); ?>">Insert <em>after</em> each item:</label>
				<input class="widefat" id="<?php echo $this->get_field_id('insert_after_item'); ?>" name="<?php echo $this->get_field_name('insert_after_item'); ?>" type="text" value="<?php echo $insert_after_item; ?>" />
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('insert_before_group'); ?>">Insert <em>before</em> group:</label>
				<input class="widefat" id="<?php echo $this->get_field_id('insert_before_group'); ?>" name="<?php echo $this->get_field_name('insert_before_group'); ?>" type="text" value="<?php echo $insert_before_group; ?>" />
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('insert_after_group'); ?>">Insert <em>after</em> group:</label>
				<input class="widefat" id="<?php echo $this->get_field_id('insert_after_group'); ?>" name="<?php echo $this->get_field_name('insert_after_group'); ?>" type="text" value="<?php echo $insert_after_group; ?>" />
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('number_items'); ?>">Number of items:</label>
				<input class="widefat" id="<?php echo $this->get_field_id('number_items'); ?>" name="<?php echo $this->get_field_name('number_items'); ?>" type="text" value="<?php echo $number_items; ?>" /><br /><em>Leave blank for all</em>
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('add_more'); ?>">
				<input type="checkbox" id="<?php echo $this->get_field_id('add_more'); ?>" name="<?php echo $this->get_field_name('add_more'); ?>" value="yes" <?php echo $add_more == 'yes' ? ' checked="checked"' : ''; ?> /><span>Add 'Read More' link</span>
				</label>
			</div>
		</div>

		<?php
    }


} // class emuRecentPosts



?>