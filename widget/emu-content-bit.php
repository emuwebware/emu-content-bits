<?php

/*
Emu Widget: Emu Content Bit
Emu Widget Class: emuContentBitWidget
Emu Widget Description: Add content bits
*/

/**
 * emuContentBitWidget Class
 */
class emuContentBitWidget extends WP_Widget
{
	public $menuClass;
	public $fiOptions = array( 'before' => 'Before content', 'after' => 'After content' );
	public $sourceOptions = array( 'body' => 'Main Body', 'excerpt' => 'Excerpt', 'title' => 'Title' );

	public $currentInstance;
	public $currentArgs;

	private $disableFilters = false;

	/** constructor */
	function emuContentBitWidget()
	{
		global $emuContentBits;

		parent::WP_Widget( false, $name = 'Emu Content Bit' );
	}

    function widget($args, $instance) {

		global $wpdb, $blog_id; extract( $args );

		$this->currentArgs = $args;
		$this->currentInstance = $instance;

		$title 				= apply_filters( 'widget_title', @$instance['title'] );
		$post_id 			= $instance['post_id'];
		$bit_id 			= $instance['bit_id'];
		$page_id			= $instance['page_id'];
		$attachment_id		= $instance['attachment_id'];
		$insert_before		= $instance['insert_before'];
		$insert_after		= $instance['insert_after'];
		$image_size			= $instance['image_size'];
		$disable_filters	= $instance['disable_filters'];

		$this->disableFilters = $disable_filters;

		if( empty( $image_size ) ) $image_size = 'large';

		$content = $this->prepPostContent( $bit_id );
		$content .= $this->prepPostContent( $post_id );
		$content .= $this->prepPostContent( $page_id );

		if( $attachment_id )
			$content .= wp_get_attachment_image( $attachment_id, $image_size );

		echo $before_widget;
		if ( $title ) echo $before_title . $title . $after_title;
		echo $insert_before . $content . $insert_after;
		echo $after_widget;

	}

	function prepPostContent( $post_id = null )
	{
		if( !$post_id ) return '';

		$content = '';

		$instance = $this->currentInstance;

		$insert_fi		= $instance['insert_fi'];
		$content_source	= $instance['content_source'];
		$link_image		= @$instance['link_image'] == 'yes';
		$add_more		= @$instance['add_more'] == 'yes';
		$image_size		= $instance['image_size'];

		$permalink = '<a href="'.get_permalink( $post_id ).'">%s</a>';

		if( $insert_fi )
		{
			$featured_image = $this->getFeaturedImage( $post_id, $image_size );
			if( $link_image ) $featured_image = sprintf( $permalink, $featured_image );
		}

		if( $insert_fi == 'before') $content .= $featured_image;
		$content .= $this->get_content( $post_id, $content_source );
		if( $insert_fi == 'after') $content .= $featured_image;

		$read_more = apply_filters( 'emu_content_bit_read_more', 'Read More' );

		if( $add_more ) $content .= '<p class="read-more"><a href="'.get_permalink( $post_id ).'">'.$read_more.'</a></p>';

		return $content;
	}

    function update($new_instance, $old_instance) { return $new_instance; }

	function getFeaturedImage( $post_id, $image_size )
	{
		if( empty( $image_size ) ) $image_size = 'large';
		return get_the_post_thumbnail( $post_id, $image_size );
	}

	function get_content( $post_id, $content_source )
	{
		$post = get_page ( $post_id );

		switch( $content_source )
		{
			case 'excerpt':
				$content = $post->post_excerpt;
				if( !$this->disableFilters) $content = apply_filters( 'the_excerpt', $content );
			break;

			case 'title':
				$content = $post->post_title;
				if( !$this->disableFilters) $content = apply_filters( 'the_title', $content );
			break;

			case 'body':
			default:
				$content = $post->post_content;
				if( !$this->disableFilters) $content = apply_filters( 'the_content', $content );
			break;

		}

		$content = apply_filters_ref_array( 'emu_content_widget_content', array( $content, $post, $this ) );

		return $content;
	}

	function getImageSizes()
	{
		global $_wp_additional_image_sizes;

		$arr_image_sizes = array();

		foreach ( get_intermediate_image_sizes() as $size )
		{
			if ( isset( $_wp_additional_image_sizes[$size]['width'] ) ) // For theme-added sizes
				$width = intval( $_wp_additional_image_sizes[$size]['width'] );
			else                                                     // For default sizes set in options
				$width = get_option( "{$size}_size_w" );

			if ( isset( $_wp_additional_image_sizes[$size]['height'] ) ) // For theme-added sizes
				$height = intval( $_wp_additional_image_sizes[$size]['height'] );
			else                                                      // For default sizes set in options
				$height = get_option( "{$size}_size_h" );

			$arr_image_sizes[$size] = "$size ({$width}x{$height})";

		}

		return $arr_image_sizes;

	}


	function form($instance) {

		$title 				= esc_attr( @$instance['title'] );
		$bit_id				= esc_attr( @$instance['bit_id'] );
        $post_id 			= esc_attr( @$instance['post_id'] );
		$page_id			= esc_attr( @$instance['page_id'] );
		$attachment_id		= esc_attr( @$instance['attachment_id'] );
		$insert_before		= esc_attr( @$instance['insert_before'] );
		$insert_after		= esc_attr( @$instance['insert_after'] );
		$insert_fi			= esc_attr( @$instance['insert_fi'] );
		$content_source		= esc_attr( @$instance['content_source'] );
		$link_image			= esc_attr( @$instance['link_image'] );
		$add_more			= esc_attr( @$instance['add_more'] );
		$image_size			= esc_attr( @$instance['image_size'] );
		$disable_filters 	= esc_attr( @$instance['disable_filters'] );

		$option_format = '<option value="%s"%s>%s</option>';

		// Get a list of content bits
		$all_bits = get_posts( 'numberposts=-1&post_type=content-bit' ); $bits_dd = '';

		foreach( $all_bits as $post )
		{
			$selected = strcmp( $post->ID, $bit_id ) == 0 ? ' selected="selected"' : '';

			// Build the posts drop down, limit the number of characters for the title (25 characters)
			$bits_dd .= sprintf( $option_format, $post->ID, $selected, preg_replace( '/^(.{25})(.{1,})/', '$1...', $post->post_title ) );
		}


		// Get a list of posts
		$all_posts = get_posts( 'numberposts=-1' ); $posts_dd = '';

		foreach( $all_posts as $post )
		{
			$selected = strcmp( $post->ID, $post_id ) == 0 ? ' selected="selected"' : '';

			// Build the posts drop down, limit the number of characters for the title (25 characters)
			$posts_dd .= sprintf( $option_format, $post->ID, $selected, preg_replace( '/^(.{25})(.{1,})/', '$1...', $post->post_title ) );
		}

		// Get a list of pages
		$all_pages = get_pages(); $pages_dd = '';

		foreach( $all_pages as $page ) {

			$selected = strcmp( $page->ID, $page_id ) == 0 ? ' selected="selected"' : '';

			// Build the pages drop down
			$pages_dd .= sprintf($option_format, $page->ID, $selected, preg_replace('/^(.{25})(.{1,})/', '$1...', $page->post_title));
		}

		// Get a list of attachments
		$args = array(
			'post_type' => 'attachment',
			'numberposts' => -1,
			'post_status' => null,
			'post_parent' => null, // any parent
			);

		$attachments = get_posts( $args );

		$arr_attachments = array();

		if ( $attachments )
		{
			foreach ( $attachments as $post )
			{
				if( preg_match( '/image/', $post->post_mime_type ) )
					$arr_attachments[$post->ID] = $post->post_title;
			}
		}

		?>
		<div class="emu-widget-control">
			<div>
				<label for="<?php echo $this->get_field_id('title'); ?>">
				Title (optional):</label>
				<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
			</div>
			<div>
				<label><strong>Content Bit</strong>:</label>
				<select id="<?php echo $this->get_field_id('bit_id'); ?>" name="<?php echo $this->get_field_name('bit_id'); ?>" class="widefat">
				<option value="">None</option>
				<?php echo $bits_dd?>
				</select>
			</div>
			<div>
				<label><strong>Post</strong>:</label>
				<select id="<?php echo $this->get_field_id('post_id'); ?>" name="<?php echo $this->get_field_name('post_id'); ?>" class="widefat">
				<option value="">None</option>
				<?php echo $posts_dd?>
				</select>
			</div>
			<div>
				<label><strong>Page</strong>:</label>
				<select id="<?php echo $this->get_field_id('page_id'); ?>" name="<?php echo $this->get_field_name('page_id'); ?>" class="widefat">
				<option value="">None</option>
				<?php echo $pages_dd?>
				</select>
			</div>
			<div>
				<label><strong>Image</strong>:</label>
				<?php echo drop_down( $this->get_field_id('attachment_id'),$this->get_field_name('attachment_id'), 'widefat', $attachment_id, $arr_attachments, 'None' ) ?>
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('insert_before'); ?>">Insert <em>before</em> content (optional):</label>
				<input class="widefat" id="<?php echo $this->get_field_id('insert_before'); ?>" name="<?php echo $this->get_field_name('insert_before'); ?>" type="text" value="<?php echo $insert_before; ?>" />
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('insert_after'); ?>">Insert <em>after</em> content (optional):</label>
				<input class="widefat" id="<?php echo $this->get_field_id('insert_after'); ?>" name="<?php echo $this->get_field_name('insert_after'); ?>" type="text" value="<?php echo $insert_after; ?>" />
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('insert_fi'); ?>">Insert featured image:</label>
				<?php echo drop_down( $this->get_field_id('insert_fi'),$this->get_field_name('insert_fi'), '', $insert_fi, $this->fiOptions, 'No' ) ?>
			</div>
			<div>
				<label>Image Size:</label>
				<?php echo drop_down( $this->get_field_id('image_size'),$this->get_field_name('image_size'), 'widefat', $image_size, $this->getImageSizes(), '' ) ?>
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('content_source'); ?>">Content source:</label>
				<?php echo drop_down( $this->get_field_id('content_source'),$this->get_field_name('content_source'), '', $content_source, $this->sourceOptions ) ?>
			</div>
			<div>
				<label for="<?php echo $this->get_field_id('link_image'); ?>">
				<input type="checkbox" id="<?php echo $this->get_field_id('link_image'); ?>" name="<?php echo $this->get_field_name('link_image'); ?>" value="yes" <?php echo $link_image == 'yes' ? ' checked="checked"' : ''; ?> /><span>Link featured image to post</span>
				</label>
				<label for="<?php echo $this->get_field_id('add_more'); ?>">
				<input type="checkbox" id="<?php echo $this->get_field_id('add_more'); ?>" name="<?php echo $this->get_field_name('add_more'); ?>" value="yes" <?php echo $add_more == 'yes' ? ' checked="checked"' : ''; ?> /><span>Add 'Read More' link</span>
				</label>
				<label for="<?php echo $this->get_field_id('disable_filters'); ?>">
				<input type="checkbox" id="<?php echo $this->get_field_id('disable_filters'); ?>" name="<?php echo $this->get_field_name('disable_filters'); ?>" value="yes" <?php echo $disable_filters == 'yes' ? ' checked="checked"' : ''; ?> /><span>Disable WP filters</span>
				</label>
			</div>
		</div>

		<?php
    }

}



?>