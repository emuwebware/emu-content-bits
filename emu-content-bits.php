<?php
/*
Plugin Name: Emu Content Bits
Plugin URI: http://www.emuwebware.com
Description: Emu wants content
Version: 0.3
Author: Emu
Author URI: http://www.emuwebware.com
*/

add_action( 'emu_framework_loaded', 'loadEmuContentBits', 1 );

function loadEmuContentBits()
{
	include_once( 'class/main.class.php' );
		
	global $emuContentBits;
	
	$emuContentBits = new emuContentBits( __FILE__ );
}

?>
