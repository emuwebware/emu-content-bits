<?php

class emuContentBits extends emuApp
{
	function __construct( $file_path ) 
	{
		$this->emuAppID = 'emuCB';
		$this->menuName = 'Emu Content Bits';
		$this->dbPrefix = 'emu_content_bit_';
		
		parent::__construct( $file_path );

		$this->loadManager( 'contentbit', 'emuContentBitManager', 'manage.contentbits.class.php' );

		$this->checkInstall();
	}
}

?>