<?php

class emuContentBitManager extends emuManager
{
	public $pages;
	
    public function __construct( $emuContentBits = null )
    {
		parent::__construct( $emuContentBits );
	}

	function registerCustomPostTypes() 
	{
		$labels = array(
			'name' => 'Content Bits',
			'singular_name' => 'Content Bit',
			'add_new' => 'Add New',
			'add_new_item' => 'Add New Content Bit',
			'edit_item' => 'Edit Content Bit',
			'new_item' => 'New Content Bit',
			'view_item' => 'View Content Bit',
			'search_items' => 'Search Content Bits',
			'not_found' => 'No content bits found',
			'not_found_in_trash' => 'No content bits found in Trash', 
			'parent_item_colon' => ''
		);
		
		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true, 
			'query_var' => true,
			'rewrite' => array('slug' => 'content-bit'),
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => 4,
			'supports' => array( 'title', 'excerpt', 'editor', 'author', 'custom-fields', 'revisions', 'thumbnail' ),
		); 
		
		register_post_type( 'content-bit', $args );
		
		register_taxonomy( 'bit-category', 'content-bit' , array( 'label' => 'Bit Categories', 'show_ui' => true, 'hierarchical' => true, 'rewrite' => array( 'slug' => 'bit-category' ), 'query_var' => true ) );
	}	
}


?>